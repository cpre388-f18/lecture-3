package edu.iastate.jmay.lecture3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

/**
 * An MVC controller between the MVC view (Android layout) and the MVC model.
 */
public class MainActivity extends AppCompatActivity {
    /// An instance variable holding one light bulb model.
    private LightBulb mBulb;
    /**
     * An instance view storing a reference to the TextView.  It's an instance variable to avoid
     * repeated calls to findViewById() in other functions.
     */
    TextView mLightStatusTextView;

    /// Treat this like a constructor and initialize all instance variables.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // The next two lines were created by Android Studio.  Don't modify them.
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Construct an instance of the MVC model.
        mBulb = new LightBulb();
        // Find the Android layout TextView.
        mLightStatusTextView = findViewById(R.id.lightStatusTextView);
        // Initialize the MVC view (layout)
        updateStatusDisplay();
    }

    /**
     * Event handler for the "Turn On" button click.
     * @param v the View that raised the event (should always be onButton)
     */
    public void onOnButtonClick(View v) {
        mBulb.turnOn();
        updateStatusDisplay();
    }

    /**
     * Event handler for the "Turn Off" button click.
     * @param v the View that raised the event (should always be offButton)
     */
    public void onOffButtonClick(View v) {
        mBulb.turnOff();
        updateStatusDisplay();
    }

    /**
     * A helper function for updating the TextView with the current state of the light.
     */
    private void updateStatusDisplay() {
        // Look at app\res\values\strings.xml for context of what is happening here.
        String lightState;
        // Query the MVC model.
        if (mBulb.isOn()) {
            lightState = getString(R.string.light_on);
        } else {
            lightState = getString(R.string.light_off);
        }
        // The overload of getString() being used treats the first argument (light_status) as a
        // C-style format string, like with printf().  lightState is the string argument used to
        // populate the "%s" in the light_status string.
        // Update the MVC view (Android layout).
        mLightStatusTextView.setText(getString(R.string.light_status, lightState));
    }
}
