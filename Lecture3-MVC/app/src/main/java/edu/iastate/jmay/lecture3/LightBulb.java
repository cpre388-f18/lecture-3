package edu.iastate.jmay.lecture3;

/**
 * A MVC model of a light bulb.
 */
public class LightBulb {
    /**
     * Instance variable storing all the necessary state of the light bulb required to implement
     * the turnOn(), turnOff(), and isOn() APIs.
     */
    private boolean mLightOn = false;

    /// Turns the light bulb on.
    public void turnOn() {
        mLightOn = true;
    }

    /// Turns the light bulb off.
    public void turnOff() {
        mLightOn = false;
    }

    /**
     * Gets the current state of the light bulb.
     * @return true if the light is on; false if the light is off
     */
    public boolean isOn() {
        return mLightOn;
    }
}
